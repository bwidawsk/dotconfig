# Bash Completion
if [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
fi

function dashj()
{
	local CPUS="$(grep -c ^processor /proc/cpuinfo)"
	echo "-j$((CPUS*2))"
}

function sleep_off()
{
	sudo systemctl mask sleep.target suspend.target
}

function sleep_on()
{
	sudo systemctl unmask sleep.target suspend.target
}

function ccache_env()
{
	path_remove /usr/lib/icecream/libexec/icecc/bin/
	path_prepend /usr/lib/ccache/bin/
}

function ice_env()
{
	path_remove /usr/lib/ccache/bin/
	path_prepend /usr/lib/icecream/libexec/icecc/bin/
}

function ltags()
{
	make $(dashj) gtags O=. SRCARCH=x86 COMPILED_SOURCE=compiled
	make $(dashj) cscope
}

function ltags_src()
{
	make $(dashj) gtags O=. SRCARCH=x86
	make $(dashj) cscope
}

kcheckpatch ()
{
	local dic=/usr/lib/python3.10/site-packages/codespell_lib/data/dictionary.txt;
	./scripts/checkpatch.pl -q -g HEAD --strict --codespell --codespellfile=$dic
}

upstream_kcheckpatch()
{
	local dic=/usr/lib/python3.10/site-packages/codespell_lib/data/dictionary.txt;
	./scripts/checkpatch.pl -q -g $(git upstream)..HEAD --strict --codespell --codespellfile=$dic
}

function test_kernel()
{
	make $(dashj) EXTRA_CFLAGS="-Werror" mm
	git-clang-format -v --diff HEAD^ | grep -q "did not\|no modified"
	./scripts/checkpatch.pl -g HEAD --strict --codespell --codespellfile=/usr/lib/python3.10/site-packages/codespell_lib/data/dictionary.txt
}
function test_cxl ()
{
	~/scripts/cxl_build_test.sh
	${*};
	kcheckpatch
}

function test_qemu()
{
	cd ~/work/clk/qemu/build
	make $(dashj)
	git-clang-format -v --diff HEAD^ | grep -q "did not\|no modified"
}

function acpi_dump()
{
	local base_path="/sys/firmware/acpi/tables/"

	if [[ ! $* ]]; then
		tables=( /sys/firmware/acpi/tables/* )
	else
		for name in "$@"; do
			tables+=( "$base_path/${name^^*}" )
		done
	fi

	for table in ${tables[@]}; do
		sudo test -d $table && continue
		sudo iasl -vi -p ${PWD}/${table##*/} -d $table
	done
}

ccache_env
