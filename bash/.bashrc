export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

if [[ $- != *i* ]]; then
	# Shell is non-interactive. Done.
	return
fi

function sourceit {
	if [ -f "$1" ]; then
		# shellcheck source=/dev/null
		. "$1"
	fi
}

function install_scripts() {
	local dotfiles="${1:-"$HOME/dotconfig/"}"
	mkdir -p "$HOME/.local/etc/bash/"
	ln -sf "$dotfiles/bash/utils.bash" "$HOME/.local/etc/bash/"
	ln -sf "$dotfiles/bash/linux.bash" "$HOME/.local/etc/bash/"
	ln -sf "$dotfiles/bash/arch.bash"  "$HOME/.local/etc/bash/"

	cp /usr/share/git/completion/git-completion.bash "$HOME/.local/bin/"
	cp /usr/share/git/completion/git-prompt.sh "$HOME/.local/bin/"
	cp /usr/share/git/git-jump/git-jump "$HOME/.local/bin/"
}

sourceit /etc/bash.bashrc
sourceit "$HOME/.aliases"
sourceit "$HOME/.local/etc/bash/utils.bash"
sourceit "$HOME/.local/bin/git-prompt.sh"
export GIT_COMPLETION_CHECKOUT_NO_GUESS=1
sourceit "$HOME/.local/bin/git-completion.bash"


# https://wiki.archlinux.org/index.php/GnuPG#SSH_agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
	SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
	export SSH_AUTH_SOCK
fi

GPG_TTY=$(tty)
export GPG_TTY
gpg-connect-agent updatestartuptty /bye >/dev/null

## Behavioral variables
export ALTERNATE_EDITOR="vim"
export BAT_PAGER='less -RF'
export BAT_STYLE='plain'
export BROWSER='firefox'
export EDITOR=nvim
export IGNOREEOF=2
export LESSCHARSET=utf-8
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"
export PAGER='bat'
export PDFVIEWER='evince'
export PROMPT_DIRTRIM=2
## History Related
export HISTCONTROL=ignoredups
export HISTIGNORE="&:ls:[bf]g:exit:pwd:clear:mount:umount:[ \t]*"
export HISTTIMEFORMAT="(%c) "
# Non-numeric values and numeric values  less  than  zero  inhibit truncation
export HISTSIZE='HUGE'
export HISTFILESIZE='HUGE'


function gui_env() {
	# GUI environment stuff
	export GDK_BACKEND=wayland     # dangerous global setting
	export CLUTTER_BACKEND=wayland # dangerous global setting
	export XDG_CURRENT_DESKTOP=sway
	export XDG_SESSION_TYPE=wayland

	#export QT_WAYLAND_FORCE_DPI=physical
	#export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
	export QT_QPA_PLATFORM=wayland-egl
	export QT_QPA_PLATFORMTHEME="kvantum"
	#export QT_SCREEN_SCALE_FACTORS="eDP-1=.8;DP-6=1"
	export QT_ENABLE_HIGHDPI_SCALING=1
	export QT_STYLE_OVERRIDE=kvantum

	export SDL_VIDEODRIVER=wayland
	export _JAVA_AWT_WM_NONREPARENTING=1

	export LIBVA_DRIVER_NAME=iHD
}

function shell_env() {
	## Shopt options
	shopt -s cdspell
	shopt -s checkjobs
	# Bash won't get SIGWINCH if another process is in the foreground.
	# Enable checkwinsize so that bash will check the terminal size when
	# it regains control.  #65623
	# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
	shopt -s checkwinsize
	shopt -s dotglob
	shopt -s expand_aliases
	shopt -s histappend
	shopt -s hostcomplete
}


## set options
set -o noclobber
set -o ignoreeof

function setup_ps1() {
	export GIT_PS1_SHOWUPSTREAM="auto"
	export GIT_PS1_SHOWDIRTYSTATE=true
	export GIT_PS1_SHOWSTASHSTATE=
	export GIT_PS1_SHOWUNTRACKEDFILES=true # Requires internet
	export GIT_PS1_SHOWCOLORHINTS=true
	export GIT_PS1_SHOWCONFLICTSTATE="yes"
	export GIT_PS1_HIDE_IF_PWD_IGNORED=true
	reset_ps1
}

function reset_ps1() {
	if [[ ${EUID} == 0 ]]; then
		PS1='\[\033[01;31m\]\h\[\033[01;34m\] \W \$\[\033[00m\] '
	else
		PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 " (\[\033[01;33m\]%s\[\033[00m\])") '
	fi
}

path_append "$HOME/.local/bin"
path_prepend "$HOME/.local/share/nvim/mason/bin/"

gui_env
shell_env

case "$(uname -s)" in
"Linux")
	sourceit "$HOME/.local/etc/bash/linux.bash"
	;;
esac

if [ -f /etc/arch-release ]; then
	sourceit "$HOME/.local/etc/bash/arch.bash"
fi

# Let the other bashrc set ps1 if they want. Otherwise, we do it.
[[ -z $set_ps1 ]] && setup_ps1 && PROMPT_COMMAND=reset_ps1

# shellcheck source=/dev/null
# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
[ "$TERM" = "xterm-kitty" ] && alias ssh="kitty +kitten ssh"

sourceit "$HOME/.cargo/env"
