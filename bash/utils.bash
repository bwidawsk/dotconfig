# http://stackoverflow.com/questions/370047/what-is-the-most-elegant-way-to-remove-a-path-from-the-path-variable-in-bash
# Linux uses some of the path functions
path_append() {
	path_remove "$1"
	export PATH="$PATH:$1"
}
path_prepend() {
	path_remove "$1"
	export PATH="$1:$PATH"
}
path_remove() {
	PATH=$(echo -n "$PATH" | awk -v RS=: -v ORS=: '"$0" != "'"$1"'"' | sed 's/:$//')
	export PATH
}

function dump() {
	od -t x4 "$1"
}

function am_wiggle() {
	git apply "$@" --reject .git/rebase-apply/patch
	local conflict_files
	conflict_files=$(git status | grep -E "rej$" | sed -e "s/^#\t\(.*\)/\1/")
	for file in $conflict_files; do
		echo wiggling in "${file%.rej}":
		rm -f "${file%.rej}".porig
		wiggle -r "${file%.rej}" "$file" || true
	done
}
