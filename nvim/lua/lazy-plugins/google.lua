local function is_goog()
    local f = io.open("/etc/lsb-release")
    if f == nil then
        return false
    end
    local lsb = f:read("*a") or ""
    f:close()
    local i = string.find(lsb, "Goobuntu")
    return i ~= nil
end

return {
    {
        url = "sso://user/fentanes/googlepaths.nvim",
        enabled = is_goog(),
        event = { "VeryLazy", "BufReadCmd //*" },
        opts = {},
    },
    {
        url = 'sso://googler@user/mccloskeybr/luasnip-google.nvim',
        enabled = is_goog(),
        dependencies = { 'L3MON4D3/LuaSnip' },
        config = function()
            require('luasnip-google').load_snippets()
        end
    },
    {
        url = "sso://user/fentanes/gcert.nvim",
        enabled = is_goog(),
        dependencies = {
            "rcarriga/nvim-notify", -- Optional
        },
        event = "VeryLazy",
        opts = {
            check_gcert_interval_ms = 10000,
            autorun_gcert = true,
            gcert_executable = "gcert",
            split_size = 12,
            show_notifications = true,
            use_nvim_notify = true,
        },
    },
    {
        url = 'sso://user/vicentecaycedo/cmp-buganizer',
        -- enabled = is_goog(),
        -- disabled because of use in blink
        enabled = false,
        dependencies = { 'nvim-lua/plenary.nvim' },
    },
    {
        url = "sso://user/rprs/buganizer.nvim",
        enabled = is_goog(),
        dependencies = {
            "nvim-telescope/telescope.nvim",
            { url = "sso://user/vicentecaycedo/buganizer-utils.nvim" },
        },
        cmd = {
            "FindBugs",
            "ShowBugUnderCursor",
        },
    },
    {
        url = 'sso://user/cassc/nvim-fmtserver',
        enabled = is_goog(),
        dependencies = {
            { url = 'sso://user/cassc/nvim-stubby' },
            { url = 'sso://user/cassc/nvim-tree-sitter-text-proto' },
            'nvim-lua/plenary.nvim',
        },
    },
}
