local M = {
    {
        "nvim-treesitter/nvim-treesitter",
        build = function()
            require("nvim-treesitter.install").update({ with_sync = true })()
        end,
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end,
        event = "BufReadPost",
        dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
        },
        opts = {
            highlight = { enable = true },
            indent = { enable = true },
            context_commentstring = { enable = true, enable_autocmd = false },
            ensure_installed = {
                "bash",
                "c",
                "cmake",
                "cpp",
                "git_config",
                "gitattributes",
                "gitcommit",
                "gitignore",
                "ini",
                "json5",
                "lua",
                "make",
                "markdown",
                "meson",
                "ninja",
                "python",
                "rust",
                "toml",
                "vim",
                "yaml",
            },
        },
        config = function(plugin, opts)
            require("nvim-treesitter.configs").setup(opts)
        end,
    },
}

return { M }
