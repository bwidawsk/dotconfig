return {
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
--    dependencies = { 'nvim-lua/lsp-status.nvim' },
    opts = function()
        local function diff_source()
            local gitsigns = vim.b.gitsigns_status_dict
            if gitsigns then
                return {
                    added = gitsigns.added,
                    modified = gitsigns.changed,
                    removed = gitsigns.removed
                }
            end
        end
        local colors = require('dracula').colors()
        return {
            options = {
                component_separators = { left = " ", right = " " },
                section_separators = { left = " ", right = " " },
                theme = 'dracula-nvim',
                globalstatus = true,
                disabled_filetypes = { statusline = {}, winbar = {} },
            },
            sections = {
                lualine_a = {
                    { "mode" },
                    { "buffer", hide_filename_extension = true, mode = 2 },
                    --{ function() return require 'lsp-status'.status() end, }
                },
                lualine_b = {
                    { 'b:gitsigns_head' },
                    { 'diff', source = diff_source }
                },
                lualine_c = {
                    {
                        "diagnostics",
                        sources = { 'nvim_lsp', 'nvim_diagnostic', 'nvim_workspace_diagnostic' },
                        symbols = {
                            error = "❌ ",
                            warn = "⚠️  ",
                            info = "🔎 ",
                            hint = "🗨 ",
                        },
                    },
                    {
                        "filetype",
                        icons_enabled = false,
                    },
                    {
                        "filename",
                        path = 3,
                    },
                },

                lualine_x = {
                    {
                        require("lazy.status").updates,
                        cond = require("lazy.status").has_updates,
                        color = { fg = colors.selection },
                        icons_enabled = false,
                    }
                },
                lualine_y = { 'progress' },
                lualine_z = {
                    {
                        "location",
                        color = { fg = colors.fg, bg = colors.none },
                    },
                },
            },
            extensions = { "aerial", "fugitive", "lazy", "mason", "fzf", "trouble", },
        }
    end,
}
