local M = {
    "neovim/nvim-lspconfig",
    lazy = false,
    dependencies = {
        { "mrcjkb/rustaceanvim",               version = '^5',            lazy = false },
        { "williamboman/mason-lspconfig.nvim", name = "mason-lspconfig" },
        { "williamboman/mason.nvim",           name = "mason" },
--        { 'nvim-lua/lsp-status.nvim',       name = "lsp_status" },
    },
    -- :h rustaceanvim.mason
    opts = {
        setup = {
            rust_analyzer = function()
                return true
            end,
        },
    },
}

M.config = function()
--    local lsp_status = require('lsp-status')
--    lsp_status.register_progress()
    require("mason").setup()
    require("mason-lspconfig").setup {
        automatic_installation = true,
    }
    local lspconfig = require('lspconfig')

    -- :h rustaceanvim.mason
    -- require("lspconfig").rust_analyzer.setup {}
    lspconfig.bashls.setup {}
    lspconfig.pylsp.setup {}
    lspconfig.yamlls.setup {}
    lspconfig.taplo.setup {}
    lspconfig.marksman.setup {}
    lspconfig.jdtls.setup {}

    lspconfig.lua_ls.setup {
        settings = {
            Lua = {
                runtime = { version = 'LuaJIT', },
                diagnostics = {
                    globals = {
                        'vim',
                        'require',
                    },
                },
                workspace = {
                    library = vim.api.nvim_get_runtime_file("", true),
                    checkThirdParty = false,
                },
                telemetry = { enable = false },
            },
        },
    }
    lspconfig.clangd.setup {
        cmd = {
            "/usr/local/google/home/bwidawsk/aluminum/prebuilts/clang/host/linux-x86/clang-r530567/bin/clangd",
            "-j",
            "16",
            "--background-index",
            "--background-index-priority=low",
--            "--clang-tidy",
            "--header-insertion=iwyu",
            "--malloc-trim",
            "--limit-references=0",
            "--limit-results=0",
            "--log=error",
            "--pch-storage=memory",
            "--rename-file-limit=0",
        },
        settings = {
            on_attach = {
                vim.keymap.set('n', 'gI', "<Cmd>ClangdSwitchSourceHeader<CR>", {silent = true})
            },
        }
        --[[
        handlers = lsp_status.extensions.clangd.setup(),
        on_attach = lsp_status.on_attach,
        capabilities = lsp_status.capabilities
        --]]
    }
end

return M
