-- mappings here and in git.lua
return function()
    local map = vim.keymap.set
    -- local opts = { nowait = true, silent = true }

    map('n', '<Leader>e', vim.diagnostic.open_float)
    map('n', '<Leader>E', vim.diagnostic.setloclist)

    vim.api.nvim_set_keymap('n', "<f11>", ":cprevious<CR>zz", {})
    vim.api.nvim_set_keymap('n', "<f12>", ":cnext<CR>zz", {})

    -- Use LspAttach autocommand to only map the following keys
    -- after the language server attaches to the current buffer
    vim.api.nvim_create_autocmd('LspAttach', {
        group = vim.api.nvim_create_augroup('UserLspConfig', {}),
        callback = function(ev)
            local function lsp_map(mode, l, r, opts)
                opts = opts or {}
                opts.buffer = ev.buf
                vim.keymap.set(mode, l, r, opts)
            end
            -- Enable completion triggered by <c-x><c-o>
            vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

            -- Buffer local mappings.
            -- See `:help vim.lsp.*` for documentation on any of the below functions
            lsp_map('n', '<Leader>gD', vim.lsp.buf.declaration)
            lsp_map('n', '<c-]>', vim.lsp.buf.definition)
            lsp_map('n', 'K', vim.lsp.buf.hover)
            lsp_map('n', '<Leader>gi', vim.lsp.buf.implementation)
            lsp_map('n', '<C-k>', vim.lsp.buf.signature_help)
            lsp_map('n', '<c-\\>c', vim.lsp.buf.incoming_calls)
            lsp_map({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action)
            lsp_map('n', '<Leader>rn', vim.lsp.buf.rename)
            lsp_map('n', '<c-\\>g', vim.lsp.buf.references)
            lsp_map('n', '<space>f', function()
                vim.lsp.buf.format { async = true }
            end)
            lsp_map('v', '<leader>vf', function()
                vim.lsp.buf.format {
                    async = true,
                    ["start"] = vim.api.nvim_buf_get_mark(0, "<"),
                    ["end"] = vim.api.nvim_buf_get_mark(0, ">"),
                }
            end)
            lsp_map('n', '<leader>it', function()
                local inlay = not vim.lsp.inlay_hint.is_enabled()
                vim.lsp.inlay_hint.enable(inlay, {})
            end)
        end,
    })
end
