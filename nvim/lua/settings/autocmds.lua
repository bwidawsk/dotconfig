return function()
    local aucmd = vim.api.nvim_create_autocmd
    local augroup = vim.api.nvim_create_augroup
    local opts = { clear = true }
    local id = {}


    id.FormatOptions = augroup('FormatOptions', opts)
    aucmd('FileType', {
        group = id.FormatOptions,
        callback = function()
            vim.opt.formatoptions = vim.opt.formatoptions
                - 'a' -- Dont format pasted code
                - 'o' -- O and o don't continue comments
                - 'r' -- Return does not continue comments
                + 't' -- Autowrap respecting textwidth
                + 'c' -- comments respect textwidth
                + 'q' -- Allow formatting comments w/ gq
                + 'n' -- Recognize numbered lists
                + 'j' -- Auto-remove comments if possible.
                + '2' -- Indent according to 2nd line
        end,
        desc = 'Custom formatoptions',
    })

    --[[
    id.DiagnosticList = augroup('DiagnosticList', opts)
    aucmd('DiagnosticChanged', {
        group = id.DiagnosticList,
        callback = function()
            vim.diagnostic.setloclist { open = false }
        end,
        desc = 'Send diagnostics to loclist on new errors',
    })
    --]]

    id.TextYank = augroup('TextYank', opts)
    -- ************** HighlightOnYank ---------------------------------------------------------
    aucmd('TextYankPost', {
        group = id.TextYank,
        callback = function()
            vim.highlight.on_yank { higroup = 'IncSearch', timeout = 200 }
        end,
        desc = 'Highlight yanked text',
    })
end
