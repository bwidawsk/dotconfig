local o = vim.o
local opt = vim.opt

return function()
    --vim.cmd.colorscheme "dracula"
    vim.g.mapleader = ","

    local tab = 4
    o.title = true
    o.expandtab = true
    o.smartcase = true
    o.ignorecase = true
    o.shiftround = true
    o.splitbelow = true
    o.splitright = true
    o.termguicolors = true
    o.hlsearch = false
    o.tabstop = tab
    o.shiftwidth = tab
    o.softtabstop = tab
    o.cmdheight = 0
    o.laststatus = 3
    o.grepformat = '%f:%l:%c:%m'
    o.grepprg = 'rg --vimgrep --smart-case --hidden'

    opt.completeopt = { 'menuone', 'noselect', 'noinsert' }

    opt.guifont = {
        'FiraCode Nerd Font:style=Medium:h12',
        'Noto Sans Devanagari:style=Medium:h10',
    }
    opt.fillchars = {
        fold = ' ',
        foldopen = '▾',
        foldsep = '│',
        foldclose = '▸',
        horiz = '━',
        horizup = '┻',
        horizdown = '┳',
        stlnc = '»',
        vert = '┃',
        vertleft = '┫',
        vertright = '┣',
        verthoriz = '╋',
    }

    opt.shortmess:append 'cs'
    opt.wildoptions:append 'fuzzy'
    opt.diffopt:append 'linematch:60'
    opt.clipboard:append 'unnamedplus'
    opt.sessionoptions:append 'terminal,tabpages'

    -- remove diagnostic jitter
    vim.wo.signcolumn = "yes"
    vim.cmd([[autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })]])

    vim.g.loaded_ruby_provider = 0
    vim.g.loaded_perl_provider = 0
    vim.g.loaded_node_provider = 0

    -- enable inlay hints by default.
    vim.lsp.inlay_hint.enable(true, {})

    -- https://neovim.io/doc/user/diagnostic.html#vim.diagnostic.config()
    vim.diagnostic.config {
        underline = true,
        virtual_text = {
            prefix = "",
            severity = nil,
            source = "if_many",
            format = nil,
        },
        signs = true,
        severity_sort = true,
        update_in_insert = false,
    }
    vim.diagnostic.enable(true)
end
