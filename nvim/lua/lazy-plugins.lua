-- Put simply configured plugins here.
return {
    {
        url = "https://gitlab.freedesktop.org/bwidawsk/goto-cs.nvim.git",
        opts = {
            debug = false,
            repository = "https://source.corp.google.com/h/googleplex-android/",
            xdg_open = false,
        }
    },
    { 'Mofiqul/dracula.nvim' },
    {
        'euclio/vim-markdown-composer',
        build = 'cargo build --release'
    },
    { 'ron-rs/ron.vim' },
    {
        'saecki/crates.nvim',
        tag = 'v0.4.0',
        event = "VeryLazy",
        dependencies = { 'nvim-lua/plenary.nvim' },
        config = function()
            require('crates').setup()
        end,
    },
    {
        "j-hui/fidget.nvim",
        tag = "legacy",
        event = "LspAttach",
        opts = {
            -- options
        },
    },
    "nvim-lua/popup.nvim",
    'dhruvasagar/vim-table-mode',
}
